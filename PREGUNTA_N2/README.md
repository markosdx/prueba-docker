# Prueba N1

## Nginx
Imagen basada en [Nginx:latest](https://hub.docker.com/_/nginx)

## Build
Para poder construir la imagen es necesario ejecutar 
```
docker build -t nginx_prueba_n2 . 
```

## Run
Para poder ejecutar el contenedor basado en la imagen previamente construida, ejecutar
```
docker run -v $PWD/public_html:/usr/share/nginx/html:ro -d -p 8080:80 nginx_prueba_n2 
```
