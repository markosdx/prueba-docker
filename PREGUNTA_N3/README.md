# Pregunta N3

Contenedor basado en mysql 5.7, se configura un usuario `(MYSQL_USER)` y un password para dicho usuario `(MYSQL_PASSWORD)`, se expone el puerto por defecto de mysql `3306`, para poder conectarse desde el cliente mysql. 

Para poder levantar el contenedor se debe ejecutar el siguiente comando 

```
docker run --name mysql -e MYSQL_ROOT_PASSWORD="PassWord" -e MYSQL_USER="zippy" -e MYSQL_PASSWORD="ZIppy123" -e MYSQL_DATABASE="zippy" -p 3306:3306 -d mysql:5.7
```
    